// This file contains material supporting section 3.7 of the textbook:
// "Object Oriented Software Engineering" and is issued under the open-source
// license found at www.lloseng.com 

package client;

import ocsf.client.*;
import common.*;

import java.io.*;

/**
 * This class overrides some of the methods defined in the abstract
 * superclass in order to give more functionality to the client.
 *
 * @author Dr Timothy C. Lethbridge
 * @author Dr Robert Lagani&egrave;
 * @author Fran&ccedil;ois B&eacute;langer
 * @version July 2000
 */
public class ChatClient extends AbstractClient
{
  //Instance variables **********************************************
  
  /**
   * The interface type variable.  It allows the implementation of 
   * the display method in the client.
   */
  ChatIF clientUI; 

  
  //Constructors ****************************************************
  
  /**
   * Constructs an instance of the chat client.
   *
   * @param host The server to connect to.
   * @param port The port number to connect on.
   * @param clientUI The interface type variable.
   */
  
  public ChatClient(String host, int port, ChatIF clientUI) 
    throws IOException 
  {
    super(host, port); //Call the superclass constructor
    this.clientUI = clientUI;
    openConnection();
  }

  
  //Instance methods ************************************************
    
  /**
   * This method handles all data that comes in from the server.
   *
   * @param msg The message from the server.
   */
  public void handleMessageFromServer(Object msg) 
  {
    clientUI.display(msg.toString());
  }

  /**
   * This method handles all data coming from the UI            
   *
   * @param message The message from the UI.    
   */
  public void handleMessageFromClientUI(String message)
  {
    try
    {
    	
    	if(message.startsWith("#"))
    	{
    				System.out.println("Comanda");
    				String command= message.substring(1);
    				
    				switch(command)
    				{
    				case "quit":
    					System.out.println("Face quit!");
    					quit();
    					break;
    				case "logoff":
    					System.out.println("Face logoff");
    					logOff();
    					break;
    				case "sethost </.*/>":
    					if(!isConnected())
    					{
    						System.out.println("Set Host");
    						String h= command.substring(9,command.length() -1);
    						setHost(h);
    						    						
    					}
    					else {
    						
    						System.out.println("Host not set");
    					}
    						
    					break;
    				case "setport </.*/>":
    					if(!isConnected())
    					{
    						System.out.println("Set port");
    						setPort(30236);
    						
    					}
    					else
    					{
    						System.out.println("Port not set");
    					}
    					break;
    				case "login":
    					if(!isConnected())
    					{
    						openConnection();
    					}
    					else
    						System.out.println("Already connected ");
    					break;
    				case "gethost":
    					{
    						System.out.println("Current host "+ getHost());
    					}
    					break;
    				case "getport":
    					{
    						System.out.println("Current port "+ getPort());
    					}
    					
    				}
    					    				
    				
    	}
    	else
    	{
    		sendToServer(message);
    	}
    	
    }
    catch(IOException e)
    {
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      quit();
    }
  }
  
  // logoff method
  
  int log=1;
  public void logOff()
  {
	  try
	  {
		  closeConnection();
		  log=0;
	  }
	  catch(IOException e) {}
	  
  }
  
  /**
   * This method terminates the client.
   */
  public void quit()
  {
    try
    {
      closeConnection();
    }
    catch(IOException e) {}
    System.exit(0);
  }
  
  final protected void connectionException(Exception exception) 
  {
	  System.out.println("connection closed");
	  quit();
  }
  
  
}
//End of ChatClient class
