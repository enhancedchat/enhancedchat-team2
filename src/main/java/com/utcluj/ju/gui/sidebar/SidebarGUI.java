package com.utcluj.ju.gui.sidebar;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;


public class SidebarGUI extends JFrame{
	
	private JScrollPane panel;	
	private JTable table;
	private ArrayList<String> usersArray;
	
	public static void main(String[] args){
		SidebarGUI frame  = new SidebarGUI();
		
		
	}
	
	public SidebarGUI(){
		initFrame();
		setVisible(true);
		setSize(170, 500);
		setFocusable(true);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void initFrame() {
		usersArray = new ArrayList<String>();
		panel = new JScrollPane();
		panel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(panel,new BorderLayout().CENTER);
		table = this.createTable();
		table.addMouseListener(new OnClickListener());
		panel.setViewportView(table);
	}
	
	
	public void listen(String username){
		
		this.clearTable();
		usersArray.add(username);
		this.updateTable();
	}
	
	public void removeUser(String username){
		this.clearTable();
		usersArray.remove(username);
		this.updateTable();
	}
	
	private void updateTable() {
		
		for(String user : usersArray){
			
			String[] users  = {user};
			DefaultTableModel modelRow = (DefaultTableModel) table.getModel();
			table.setRowHeight(50);
			modelRow.addRow(users);
		}
	}

	private void clearTable()
	{
	    DefaultTableModel dm = (DefaultTableModel) table.getModel();
	    dm.getDataVector().removeAllElements();
	}
	
	private JTable createTable(){
		DefaultTableModel  tableModel = new DefaultTableModel(new Object[] {"Users"}, 0)
		{
			public boolean isCellEditable(int row, int column) {
				return false;
		    	}
		};
		JTable usersTable  = new JTable(tableModel);
		
		DefaultTableCellRenderer renderCell = new DefaultTableCellRenderer();
		renderCell.setHorizontalAlignment(JLabel.CENTER);
		usersTable.getColumnModel().getColumn(0).setPreferredWidth(this.getWidth());
		usersTable.getColumnModel().getColumn(0).setCellRenderer(renderCell);
		usersTable.setBackground(Color.LIGHT_GRAY);
		usersTable.setShowVerticalLines(false);
		usersTable.setRowHeight(50);
		
		
		return usersTable;
	}
	
	public class OnClickListener extends MouseAdapter {
		
		public void mouseClicked(MouseEvent event){
			if(event.getClickCount() == 2){
				//ChatGUI chatWindow = new ChatGUI((String)table.getValueAt(table.getSelectedRow(),0));
				System.out.println("Do something");
				
				 /**
		           * TODO: chat window
		           */
			}
		}
			
		}
}
