package com.utcluj.ju.gui.login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * 
 * @author user This class represents the UI for the logIn application.
 * 
 */
public class LoginGUI extends JFrame {

  private JTextField userTextField;
  private JButton checkIfValid, logInButton;
  private JLabel userNameLabel;
  private final static String title = "Log In";
  private boolean valid = false;
  private static ArrayList<String> users;

  public static void main(String[] args) {
    users = new ArrayList<String>();
    LoginGUI log = new LoginGUI();
    log.setSize(300, 150);
    log.setVisible(true);

  }

  public LoginGUI() {
    super(title);
    setLayout(null);
    init();

  }

  public void init() {
    this.setLocation(600, 400);

    userNameLabel = new JLabel("Username:");
    userTextField = new JTextField();
    checkIfValid = new JButton("Check If Valid");
    logInButton = new JButton("LogIn");

    userNameLabel.setBounds(0, 4, 80, 60);
    add(userNameLabel);
    userTextField.setBounds(64, 25, 200, 20);

    add(userTextField);

    checkIfValid.setBounds(0, 60, 280, 20);
    add(checkIfValid);
    logInButton.setBounds(0, 83, 280, 20);
    add(logInButton);

    checkIfValid.addActionListener(new ActionListener() {

      //@Override
      public void actionPerformed(ActionEvent arg0) {
        if (!userTextField.equals("") && !users.contains(userTextField.getText())) {
          valid = true;
          users.add(userTextField.getText());
          JOptionPane.showMessageDialog(null, "The name is valid", "Succes", JOptionPane.PLAIN_MESSAGE);
        }
        else {
          valid = false;
          JOptionPane.showMessageDialog(null, "That username already exists,or you didn't enter a username", "Error!",
              JOptionPane.ERROR_MESSAGE);
        }

      }

    });

    logInButton.addActionListener(new ActionListener() {

     // @Override
      public void actionPerformed(ActionEvent e) {
        if (valid) {
          /**
           * TODO: logIn
           */
        }
        else {
          JOptionPane.showMessageDialog(null, "That username already exists,or you didn't enter a username", "Error!",
              JOptionPane.ERROR_MESSAGE);

        }

      }

    });

  }
}