package com.utcluj.ju.gui.chat;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Component;

public class ChatGUI extends JFrame {

	private String myUsername;
	private String otherUsername;
	
	private JPanel contentPane;
	private JTextField writingArea;
	private JButton sendButton;
	private JTextArea textAreaMessages;
	private JScrollPane scrollPane;
	
	public ChatGUI(String myUsername,String otherUsername) 
	{
		this.myUsername=myUsername;
		this.otherUsername=otherUsername;
		setTitle(otherUsername);
		initialize();
	}

	private void initialize()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 532, 492);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		writingArea = new JTextField();
		writingArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					setActions();
                }
			}
		});
		contentPane.add(writingArea);
		writingArea.setFont(new Font("Monospaced", Font.PLAIN, 17));
		writingArea.setBorder(new LineBorder(new Color(150, 150, 150), 2, true));
		writingArea.setBounds(20, 413, 402, 39);
		
		sendButton = new JButton("SEND");
		sendButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				setActions();
			}
		});
		sendButton.setFont(new Font("Tahoma", Font.BOLD, 16));
		sendButton.setBorder(new LineBorder(new Color(150, 150, 150), 2, true));
		sendButton.setBounds(427, 413, 75, 39);
		contentPane.add(sendButton);
		
		scrollPane = new JScrollPane((Component) null);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(20, 11, 482, 385);
		contentPane.add(scrollPane);
		
		textAreaMessages = new JTextArea();
		textAreaMessages.setEditable(false);
		textAreaMessages.setFont(new Font("Monospaced", Font.PLAIN, 17));
		textAreaMessages.setBorder(new LineBorder(new Color(150, 150, 150), 2, true));
		scrollPane.setViewportView(textAreaMessages);
	}

	public void setActions()
	{
		 String message = writingArea.getText();
	     String showMessage=showMessage(myUsername,message);
	     textAreaMessages.append(showMessage);
	     writingArea.setText("");
	 }
	 
	 
	 public String showMessage(String username, String message) 
	 {
		 if(message.length()>0)
			 return "<"+ username + ">:" + message+ "\n";
		return ""; 
	  }
	   
	 
	 public void actionPerformed(ActionEvent e) 
	 {
		 setActions();
	 }

	
	public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
		public void run() {
			try {
				ChatGUI frame = new ChatGUI("ben","other");
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	});
}	
}


