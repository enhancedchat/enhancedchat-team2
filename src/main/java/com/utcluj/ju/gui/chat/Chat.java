package com.utcluj.ju.gui.chat;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import java.awt.Component;

public class Chat implements ActionListener{

	private ChatFunctions obj = new ChatFunctions();
	private JFrame frame;
	private JTextField textField;
	private JTextArea display;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Chat window = new Chat();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Chat() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 589, 389);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("ChatBox");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		display = new JTextArea();
		display.setAutoscrolls(false);
		display.setCaretColor(Color.RED);
		display.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		display.setEditable(false);
		
				
				GridBagConstraints gbc_display = new GridBagConstraints();
				gbc_display.gridwidth = 10;
				gbc_display.gridheight = 13;
				gbc_display.insets = new Insets(0, 0, 5, 5);
				gbc_display.fill = GridBagConstraints.BOTH;
				gbc_display.gridx = 5;
				gbc_display.gridy = 1;
				frame.getContentPane().add(display, gbc_display);
		
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 10;
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 5;
		gbc_textField.gridy = 14;
		frame.getContentPane().add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton send = new JButton("Send");
		GridBagConstraints gbc_send = new GridBagConstraints();
		gbc_send.insets = new Insets(0, 0, 5, 5);
		gbc_send.gridx = 15;
		gbc_send.gridy = 14;
		frame.getContentPane().add(send, gbc_send);
		
		
		send.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		 	String message = textField.getText();
		 	String displayMessage;
		 	
	        displayMessage = obj.displayMessage("Test", message); //"Test" se inlocuieste cu userame s
	        display.append(displayMessage);
	        display.setCaretPosition(display.getDocument().getLength());

	}
}